package com.icpayment.monitoreo;


import java.io.IOException;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.sun.net.httpserver.*;



@SuppressWarnings("restriction")

public class ConsultaServicio  implements HttpHandler  {
	
	@Override
	public void handle(HttpExchange he) throws IOException {
      
		 try {
		
		  	 String response = Utils.obtenerString(he);
			 Headers rhe = he.getResponseHeaders();
	        
			 Utils.agregarCors(rhe);
			 Utils.agregarJSONHeader(rhe);
			 
	         String servicioString = Utils.getRequestValueFromJSON(response, "servicio");
	         String accionString = Utils.getRequestValueFromJSON(response, "accion");
	         String respuestaJSON = "";
	         
	         if(!((servicioString == null) && (accionString == null))) {
	        	 
	        	 Integer servicio = Integer.parseInt(servicioString);
	        	 Integer accion = Integer.parseInt(accionString);
		         boolean respuestaCorrecta = Utils.invocaServicio(servicio, accion);
	        	 
		         Gson gson = new Gson();
		         respuestaJSON = gson.toJson(new RespuestaJSON(String.valueOf(respuestaCorrecta)));
		         
	         }else {
	        	 System.out.println("Estructura del JSON invalida");
		         Gson gson = new Gson();
		         respuestaJSON = gson.toJson(new RespuestaJSON(String.valueOf(false)));
	         }
	         
	         he.sendResponseHeaders(200, respuestaJSON.length());
	         OutputStream os = he.getResponseBody();
	         os.write(respuestaJSON.getBytes());
	         os.close();
	         
		 } catch(Exception e) {	 
			 e.printStackTrace();
	         Gson gson = new Gson();
	         String respuestaJSON = gson.toJson(new RespuestaJSON(String.valueOf(false)));
	         Headers rhe = he.getResponseHeaders();
	         Utils.agregarCors(rhe);
	         he.sendResponseHeaders(500, respuestaJSON.length());
	         OutputStream os = he.getResponseBody();
	         os.write(respuestaJSON.getBytes());
	         os.close();
	         
		 }	
		
    }

}
