package com.icpayment.monitoreo;

public class RespuestaJSON {

	public RespuestaJSON(String respuesta) {
		this.respuesta = respuesta;
	}

	private String respuesta = "false";

	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
	
	
}
