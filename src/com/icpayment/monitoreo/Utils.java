package com.icpayment.monitoreo;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.Consumer;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

public class Utils {

	public static String obtenerString (HttpExchange he) {
        
		InputStream input = he.getRequestBody();
        InputStreamReader isReader = new InputStreamReader(input);
        BufferedReader reader = new BufferedReader(isReader);
        
        StringBuffer sb = new StringBuffer();
        String str = "";
         
        try {
        	
			while((str = reader.readLine())!= null){
			    sb.append(str);
			}
			     
			return sb.toString().trim();
		} catch (IOException e) {
			e.printStackTrace();
			return str;
		} catch (Exception e) {
			e.printStackTrace();
			return str;
		}
        

	}
	
	public static void agregarCors(Headers rhe) {
	    rhe.add("Access-Control-Allow-Origin", "*");
	    rhe.add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT, PATCH, HEAD");
	    rhe.add("Access-Control-Allow-Headers", "*");
	    rhe.add("Access-Control-Expose-Headers", "*");
	    rhe.add("Access-Control-Allow-Credentials", "true");
	
	}
	
	public static void agregarJSONHeader(Headers rhe) {
		rhe.add("Content-Type", "application/json");
	}
	
	public static int executeInTerminal(String command, int operativeSystem) throws IOException, InterruptedException {
	    final String[] wrappedCommand;
	    
	    switch(operativeSystem) {
		    case 1:
		        wrappedCommand = new String[]{ "cmd", "/c", "start", "/wait", "cmd.exe", "/K", command };
		        break;
		    
		    case 2:
		    	wrappedCommand = new String[]{ "xterm", "-e", "bash", "-c", command};
		    	break;
		    case 3:
		    	 wrappedCommand = new String[]{"osascript",
			                "-e", "tell application \"Terminal\" to activate",
			                "-e", "tell application \"Terminal\" to do script \"" + command + ";exit\""};
		    default:
		        throw new RuntimeException("Sistema operativo no soportado");
	    }

	    Process process = new ProcessBuilder(wrappedCommand)
	            .redirectErrorStream(true)
	            .start();
	    try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
	        String line;
	        while ((line = reader.readLine()) != null) {
	            System.out.println(line); 
	        }
	    }
	    return process.waitFor();
	}
	
	public static String executeWindowsCommandWithOutTerminal(ArrayList<String> list) throws IOException, InterruptedException {
		
       StringBuffer sb = new StringBuffer();

		try {
			ArrayList<String> commandList = new ArrayList<String>();
			commandList.add("cmd");
			commandList.add("/c");
			commandList.addAll(list);
			
			for (String valor: commandList) {
				System.out.println("Valor2: "+valor);
			}
			
			//final String[] wrappedCommand = commandList.stream().toArray(String[]::new);

			//"/c"
			final String[] wrappedCommand = new String[]{ "cmd","/c" };
		
		    Process process = new ProcessBuilder(wrappedCommand)
		            .redirectErrorStream(true)
		            .start();
		    try (BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
		        String line;
		        while ((line = reader.readLine()) != null) {
		            sb.append(line);
		        }
		    }
		    
		    process.waitFor();
		}catch(Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		
		return sb.toString();
		
	}
	

	
	   public static String execute(String command) throws InterruptedException, IOException {
		   
		
		    ExecutorService executor = Executors.newSingleThreadExecutor();
	
		    
		    Future<String> future  = executor.submit(new Callable<String>() {

			   public String call() throws Exception {
			      
				    StringBuilder texto = new StringBuilder();
					  
			        //Runtime runtime = Runtime.getRuntime();
			        
			

			        String[] commands  = {"cmd", "/c", "start", "/wait", "cmd.exe", "/K",command};
			   
			        //Process process = runtime.exec(commands);
			        
				    Process process = new ProcessBuilder(commands)
				            .redirectErrorStream(true)
				            .start();

			        BufferedReader lineReader = new BufferedReader(new InputStreamReader(process.getInputStream()));

			        Consumer<String> consumer= i-> {
			        	texto.append(i);
			        };
			        
			        lineReader.lines().forEach(consumer);

			        BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			        errorReader.lines().forEach(System.out::println);
			        
			        lineReader.close();
			        errorReader.close();
			        


			        return texto.toString();

			   }
			
			});
		    
		    
		    return "s";
	
	   }
	   
	   public static void validaPuerto(int puerto) {
	    	
	        if(isLocalPortInUse(puerto)) {
	        
		        Runtime rt = Runtime.getRuntime();
		       
		        try {
		        	
		            String line;
		            
				    Process pr = rt.exec("cmd /c netstat -ano | find \""+puerto+"\"");
					
				    BufferedReader input =  new BufferedReader (new InputStreamReader(pr.getInputStream()));  
				  
				   
				    while ((line = input.readLine()) != null) {  
				    	
				    	if(buscarSiExistePuerto(line,puerto)) {
				    		String proceso = extraeProceso(line);
				    		eliminaProceso(proceso);
				    	}
				    }  
				    
				    input.close();  
					
				} catch (IOException e1) {
					javax.swing.JOptionPane.showMessageDialog(null, e1.getMessage());
					e1.printStackTrace();
				}
	        
	        }
	    
	    }
	    
	    public static void eliminaProceso(String proceso) {
	    	
	        Runtime rt = Runtime.getRuntime();
		       
		    try {
		        	
	            String line;
		            
				Process pr = rt.exec("cmd /c taskkill -pid \""+proceso+"\" /f");
					
				BufferedReader input =  new BufferedReader (new InputStreamReader(pr.getInputStream()));  
				   
				while ((line = input.readLine()) != null) {  
				    System.out.println(line);
				}  
				    
				input.close();  
					
			}catch (IOException e1) {
				javax.swing.JOptionPane.showMessageDialog(null, e1.getMessage());
			    e1.printStackTrace();
			}
		    
	    }
	    
	    
	    public static String extraeProceso(String linea) {
	    	
	    	String[] cadena = linea.split("\\s+");
	    	
	    	ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(cadena));
	    	
	    	if(arrayList.size() == 6) {
	    		System.out.println("Proceso: "+arrayList.get(5));
	    		return arrayList.get(5);
	    	}else {
	    		return "";
	    	}
	    	
	    }
	    
	    public static boolean buscarSiExistePuerto(String cadena, int puerto) {
	        
	    	String busqueda="0.0.0.0:"+puerto;
	        
	        int intIndex = cadena.indexOf(busqueda);
	      
	        if(intIndex == - 1){
	            return false;
	        }else{
	            return true;
	        }
	        
	    }
	    
	    
	    public static boolean isLocalPortInUse(int port) {
		    try {
		        new ServerSocket(port).close();
		    
		        return false;
		    }catch(IOException e) {
		        
		    	return true;
		    
		    }
	    }
	    
	    public static void leerArchivoJSON() {
	
	    	String archivo = "rutas.txt";
	    
	        JsonObject jsonObject = new JsonObject();
	        
	        try {
	     
	            JsonElement jsonElement = JsonParser.parseReader(new FileReader(archivo));
	            jsonObject = jsonElement.getAsJsonObject();
	            JsonElement element_one = jsonObject.get("servicios");
	            
	            if(!(element_one == null)) {
	          
	            	JsonElement element_two = element_one.getAsJsonObject();
	            
		            if(element_two != null) {
		            	JsonObject jsonObject2 = element_two.getAsJsonObject();
		            	JsonElement element_three = jsonObject2.get("autenticaciontotem");
		            	String test = element_three.toString();

		            }
		            
		            
	            }
	            
	        } catch (IOException ioe){
		        	ioe.printStackTrace();
		        	javax.swing.JOptionPane.showMessageDialog(null, ioe);   
	        } catch(Exception e) {
	        	e.printStackTrace();
	        	javax.swing.JOptionPane.showMessageDialog(null, e);
	        }
	        
	    
	    }
	    
	    public static String leerServiciosJSON(String servicio, String valorSolicitado) {
	    	
	    	if(valorSolicitado== null || servicio == null) {
	    		return null;
	    	}
	    	
	    	String archivo = "rutas.txt";
	    
	        JsonObject jsonObject = new JsonObject();
	        
	        try {
	     
	            JsonElement jsonElement = JsonParser.parseReader(new FileReader(archivo));
	            jsonObject = jsonElement.getAsJsonObject();
	            JsonElement element_one = jsonObject.get("servicios");
	            
	            if(!(element_one == null)) {
	            	
	            	JsonElement element_two = element_one.getAsJsonObject();
	            	
		            if(element_two != null) {
		            	JsonObject jsonObject2 = element_two.getAsJsonObject();
		            	JsonElement element_three = jsonObject2.get(servicio);
		            	System.out.println(element_three.toString());
		            	System.out.println("valor solicitado: "+valorSolicitado);
		            	System.out.println("servicio: "+servicio);
		            	String valor = getRequestValueFromJSON(element_three.toString(), valorSolicitado);

		            	return valor;
		            }else {
		            	return null;
		            }
		            
		            
	            }else {
	            	return null;
	            }
	 
	            
	        } catch (IOException ioe){
		        ioe.printStackTrace();
		        javax.swing.JOptionPane.showMessageDialog(null, ioe);   
		        return null;
	        } catch(Exception e) {
	        	e.printStackTrace();
	        	javax.swing.JOptionPane.showMessageDialog(null, e);
	        	return null;
	        }
	        
	    
	    }
	    
	    public static String stringToJSON(String cadena) {
	    	
	    	String resultado = "";
	    	try {
		    	resultado = "";
		    	JsonObject jsonObject =JsonParser.parseString(cadena).getAsJsonObject();
		    	return jsonObject.toString();
	    	}catch(Exception e) {
	    		System.out.println(e.toString());
	    		return resultado;
	    	}

	    	
	    }
	    
	    public static boolean invocaServicio(Integer servicio, Integer accion) {
	    	
	    	boolean estado = false;
	    	
	    	switch(servicio){
	    	
	    		case 0: estado = true;
	    			    break;
	    		
	    		case 1: estado = invocarAccionAutentificationService(accion);
	    				break;
	    		
	    		case 2: estado = invocarICPersonalizationService(accion); 
	    			
	    			break;
	    		
	    		default: System.out.println("Operacion no permitida");
	    	}
	    	
	    	return estado;
	    }
	    
	    private static boolean invocarICPersonalizationService(Integer accion) {
	    	
	    	boolean estado = false;
	    	
	    	switch(accion) {
	    		case 1: estado = iniciarServicio("icpersonalization_service","ICPersonalizationService_x86.exe");
	    				break;
	    		case 2: estado = detenerServicio("icpersonalization_service");
	    				break;
	    		case 3: estado = consultarEstadoServicio("icpersonalization_service");
	    				break;
	    		default: System.out.println("Accion no permitida");
	    	}
	    	
	    	return estado;
			
		}


		private static boolean invocarAccionAutentificationService(Integer accion) {
			
			boolean estado = false;
			
	    	switch(accion) {
	    		case 1: estado = iniciarServicio("autenticaciontotem","AutenticacionTotem.exe");
	    				break;
	    		case 2: estado = detenerServicio("autenticaciontotem");
	    				break;
	    		case 3: estado = consultarEstadoServicio("autenticaciontotem");
	    				break;
	    			
	    		default: System.out.println("Accion no permitida");
			}
	    	
	    	return estado;
			
		}

		private static boolean consultarEstadoServicio(String servicio) {
			
			boolean estado = false;
			
			try {
				String puertoString = leerServiciosJSON(servicio,"puerto");
				
				Integer puerto = Integer.parseInt(puertoString);
				
				if(isLocalPortInUse(puerto)) {
					System.out.println("Puerto en uso");
					estado =  true;
				}else {
					System.out.println("Puerto libre");

				}
				
			}catch(Exception e){
				System.out.println("Error: "+e.getMessage());
				e.printStackTrace();
				return estado;
			}
			
			return estado;
			
		}

		private static boolean detenerServicio(String servicio) {
			
	       boolean estado = false;
			
		   try {
				String puertoString = leerServiciosJSON(servicio,"puerto");
				Integer puerto = Integer.parseInt(puertoString);
				
				if(isLocalPortInUse(puerto)) {
					validaPuerto(puerto);
		
				}else {
					System.out.println("Puerto libre");

				}
				estado =  true;
				
			}catch(Exception e){
				System.out.println("Error: "+e.getMessage());
				e.printStackTrace();
			}
			
			return estado;
				
		}

		private static boolean iniciarServicio(String servicio, String commando) {

			boolean estado = false;

			System.out.println("Entrando en la funcion: iniciarServicio");
			
			try {

				String urlString = leerServiciosJSON(servicio,"url");
				
		        String[] commands = {"cd "+urlString+" && "+commando};
		        
		        String resultado = "";

		        for(String command : commands) {
		            try {
						resultado = execute(command);
						if(resultado.length() > 0) {
							estado = true;
						}
					
		            } catch (InterruptedException e) {

						e.printStackTrace();
					} catch (IOException e) {

						e.printStackTrace();
					}
		        }
		        
				
			}catch(Exception e){
				System.out.println("Error: "+e.getMessage());
				e.printStackTrace();
				return estado;
			}
			
			return estado;
			
		}

		public static String getRequestValueFromJSON(String json, String clave) {

	        try {
		    
		        JsonObject jsonObject = new Gson().fromJson(json, JsonObject.class);
	
		        JsonElement elemento = jsonObject.get(clave);
		     	
		     	if(elemento == null) {
		     		System.out.println("Parametro clave invalido");
		     	}else {
		     		String valor = elemento.getAsString();
		     		
		     		if(valor==null) {
		     			return null;
		     		}
		     		
		     		return valor;

		     	}

		     	return null;
	
	        } catch(Exception e) {
	        	System.out.println(e.getMessage());
	        	//e.printStackTrace();
	        	return null;
	        }
	        
	    }
}
