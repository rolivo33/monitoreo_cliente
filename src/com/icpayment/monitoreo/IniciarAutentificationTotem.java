package com.icpayment.monitoreo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Map;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class IniciarAutentificationTotem implements HttpHandler {

	@Override
	public void handle(HttpExchange he) throws IOException {

		
		try {

            String text_response = Utils.obtenerString(he);
			Headers rhe = he.getResponseHeaders();
			
			 Utils.agregarCors(rhe);
			 Utils.agregarJSONHeader(rhe);
			
			System.out.println("SDD");
			System.out.println(text_response);

	        he.sendResponseHeaders(200, text_response.length());
	          
	        OutputStream os = he.getResponseBody();
	        os.write(text_response.getBytes());
	        os.close();
		
		}catch(Exception e) {
			javax.swing.JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
		}

		
		try {
			
			
			final String commands[] = {"cd c:/", "dir"}; 

		    Process process = new ProcessBuilder("java","-version").start(); 
		      
		    InputStream is = process.getInputStream();
		      
		    InputStreamReader isr = new InputStreamReader(is);
		      
		    BufferedReader br = new BufferedReader(isr);

		    String line;
		      
		    while ((line = br.readLine()) != null) {
		        System.out.println(line);
		    }
		}catch(Exception e) {
			javax.swing.JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
			
		}
	      

	}

}
