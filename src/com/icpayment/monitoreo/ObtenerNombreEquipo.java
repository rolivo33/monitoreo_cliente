package com.icpayment.monitoreo;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.util.HashMap;


import com.google.gson.Gson;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class ObtenerNombreEquipo implements HttpHandler{

	@Override
	public void handle(HttpExchange he) throws IOException {
      
		 try {
		
			 Headers rhe = he.getResponseHeaders();
	        
			 Utils.agregarCors(rhe);
			 Utils.agregarJSONHeader(rhe);
			 
	         String respuestaJSON = "";
	         
	         InetAddress address = InetAddress.getLocalHost();
	         String sHostName = address.getHostName();
	         HashMap<String, String> jsonResponse = new HashMap<String, String>();
	         jsonResponse.put("nombre_equipo",sHostName);
	         
	         Gson gson = new Gson();
	         respuestaJSON = gson.toJson(jsonResponse);
	         
	         he.sendResponseHeaders(200, respuestaJSON.length());
	         OutputStream os = he.getResponseBody();
	         os.write(respuestaJSON.getBytes());
	         os.close();
	         
		 } catch(Exception e) {	 
			 e.printStackTrace();
	         Gson gson = new Gson();
	         String respuestaJSON = gson.toJson(new RespuestaJSON(String.valueOf(false)));
	         Headers rhe = he.getResponseHeaders();
	         Utils.agregarCors(rhe);
	         he.sendResponseHeaders(500, respuestaJSON.length());
	         OutputStream os = he.getResponseBody();
	         os.write(respuestaJSON.getBytes());
	         os.close();
	         
		 }	
		
    }

}
