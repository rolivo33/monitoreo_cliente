package com.icpayment.monitoreo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Arrays;

import com.sun.net.httpserver.*;

public class Monitoreo {
	

    public static void main(String[] args) {

        int puerto = 9000;
	    
        Utils.validaPuerto(puerto);
        
	    try {
  
            HttpServer server =  HttpServer.create(new InetSocketAddress(puerto), 0);
	    	server.createContext("/test", new ManejadorTest());
	    	server.createContext("/consultaServicio", new ConsultaServicio());
	    	server.createContext("/iniciarAutentificationTotem",new IniciarAutentificationTotem());
	    	server.createContext("/obtenerNombreEquipo",new ObtenerNombreEquipo());
	    	server.setExecutor(null);
	    	server.start();
	        System.out.println("Conectado... ");
	       		
		} catch (IOException e) {
			javax.swing.JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
		}  
    }
    
    
 

}
